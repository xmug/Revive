using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SimpleFramework
{
    public interface IBindableProperty
    {
        
    }
    
    public class BindableProperty<T> : IBindableProperty where T : new()
    {
        private T _value;

        public BindableProperty(T defaultValue = default(T))
        {
            _value = new T();
        }

        public T Value
        {
            get => _value;
            set
            {
                if (!EqualityComparer<T>.Default.Equals(value, _value))
                {
                    _value = value;
                    
                    OnValueChanged?.Invoke(_value);
                }
            }
        }

        public Action<T> OnValueChanged;
    }
}