﻿namespace SimpleFramework
{
    public interface BaseCoreComponent
    {
        bool IsInit { get; set; }
        void Init();
    }
}