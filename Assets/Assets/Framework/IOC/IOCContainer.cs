﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SimpleFramework
{
    public class IOCContainer
    {
        // 以类型为key，以对象为值
        private Dictionary<Type, object> _instances = new Dictionary<Type, object>();

        public void Register<T>(T instance)
        {
            var key = typeof(T);

            // 相同的类，在容器内只能存在一个（Static)
            if (_instances.ContainsKey(key))
            {
                // 如果已经存在，就刷新实例对象
                _instances[key] = instance;
            }
            else
            {
                _instances.Add(key, instance);
            }
        }

        public T Get<T>() where T : class, BaseCoreComponent
        {
            // 从字典中取出对应类型实例
            var key = typeof(T);

            if (_instances.TryGetValue(key, out var retInstance))
            {
                return retInstance as T; // 将object对象显式转化为T类型
            }

            return null;
        }
    }
}