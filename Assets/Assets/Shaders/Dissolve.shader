// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/Ramp texture"
{
    Properties
    {
        _RampTex("Ramp texture", 2D) = "white"
        _Gloss("Gloss", float) = 20
    }

    SubShader
    {
        Pass
        {
            Tags
            {
                "LightMode" = "ForwardBase"
            }

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "Lighting.cginc"

            struct v2f
            {
                float4 pos : SV_POSITION;
                float4 worldPos : TEXCOORD0;
                float3 worldNormal : TEXCOORD1;
            };

            v2f vert(appdata_base v)
            {
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                o.worldNormal = normalize(UnityObjectToWorldNormal(v.normal));
                return o;
            }

            sampler2D _RampTex;
            float _Gloss;

            float4 frag(v2f i) : SV_TARGET
            {
                float3 worldLightDir = normalize(UnityWorldSpaceLightDir(i.worldPos));
                float3 worldViewDir = normalize(UnityWorldSpaceViewDir(i.worldPos));

                float halfLambert = dot(worldLightDir, i.worldNormal) * 0.5 + 0.5;
                float3 diff = _LightColor0.rgb * tex2D(_RampTex, float2(halfLambert, halfLambert));

                float3 halfDir = normalize(worldLightDir + worldViewDir);
                float3 spec = _LightColor0.rgb * pow(saturate(dot(halfDir, i.worldNormal)), _Gloss);

                float3 col = spec + diff + UNITY_LIGHTMODEL_AMBIENT.rgb;

                return float4(col.rgb ,1);
            }

            ENDCG
        }
    }

    fallback "Diffuse"
}