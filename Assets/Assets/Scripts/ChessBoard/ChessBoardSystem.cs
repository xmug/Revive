﻿using System;
using System.Collections.Generic;
using Assets.Scripts.Utility;
using SimpleFramework;
using UnityEngine;

namespace Assets.Scripts
{
    public class ChessBoardSystem : ISystem
    {
        public Dictionary<CustomPos, SingleChessBlock> SmallChessBoard;
        public Dictionary<CustomPos, SingleChessBlock> LargeChessBoard;
        public ReviveModel model;
        public EventSystem eSys;

        public SingleChessBlock GetChessBlock(CustomPos pos, ChessBoardType type, bool ignoreAbleLimit = false)
        {
            SingleChessBlock rt;
            if (type == ChessBoardType.large)
            {
                LargeChessBoard.TryGetValue(pos, out rt);
            }
            else
            {
                SmallChessBoard.TryGetValue(pos, out rt);
            }

            if (rt != null && !rt.IsAbleToStepOn)
            {
                if (ignoreAbleLimit)
                    return rt;
                return null;
            }
            return rt;
        }

        public void RegisterChessBlock(SingleChessBlock chessBlock, ChessBoardType type)
        {
            Dictionary<CustomPos, SingleChessBlock> dic
                = type == ChessBoardType.small ? SmallChessBoard : LargeChessBoard;

            if (dic == null)
                dic = new Dictionary<CustomPos, SingleChessBlock>();

            if (dic.ContainsKey(chessBlock.myPos))
                return;

            dic.Add(chessBlock.myPos, chessBlock);
        }

        public bool IsInit { get; set; }

        public void Init()
        {
            // Init
            SmallChessBoard = new Dictionary<CustomPos, SingleChessBlock>();
            LargeChessBoard = new Dictionary<CustomPos, SingleChessBlock>();
            model = ReviveCore.Get<ReviveModel>();
            eSys = ReviveCore.Get<EventSystem>();

            // model.curPlayerPos.Value.Print();
            // // 手动测算一次，之后靠事件同步
            // CalculateAbleBlocks(model.curPlayerPos.Value, ChessBoardType.small);
            // CalculateAbleBlocks(model.curPlayerDummyPos.Value, ChessBoardType.large);

            // 注册事件
            model.curPlayerPos.OnValueChanged += pos => CalculateAbleBlocks(pos, ChessBoardType.small);
            model.curPlayerDummyPos.OnValueChanged += pos => CalculateAbleBlocks(pos, ChessBoardType.large);
        }

        private void CalculateAbleBlocks(CustomPos pos, ChessBoardType type)
        {
            Dictionary<CustomPos, SingleChessBlock> dic =
                type == ChessBoardType.small ? SmallChessBoard : LargeChessBoard;

            List<SingleChessBlock> canStepBlocks =
                type == ChessBoardType.small ? model.smallCanStepBlocks : model.largeCanStepBlocks;

            // 清除
            foreach (var canStepBlock in canStepBlocks)
            {
                canStepBlock.IsAbleToStepOn = false;
            }

            canStepBlocks.Clear();

            var Cur = new CustomPos()
            {
                x = pos.x,
                y = pos.y
            };

            var Up = new CustomPos()
            {
                x = pos.x,
                y = pos.y - 1
            };

            var Down = new CustomPos()
            {
                x = pos.x,
                y = pos.y + 1
            };

            var Left = new CustomPos()
            {
                x = pos.x - 1,
                y = pos.y
            };

            var Right = new CustomPos()
            {
                x = pos.x + 1,
                y = pos.y
            };

            CustomPos[] positions = new CustomPos[5];
            positions[0] = Cur;
            positions[1] = Up;
            positions[2] = Down;
            positions[3] = Left;
            positions[4] = Right;

            SingleChessBlock block = null;
            foreach (var position in positions)
            {
                if (position.Equals(Cur))
                    continue;
                if (dic.TryGetValue(position, out block))
                {
                    if (type == ChessBoardType.small
                        && model.smallBlockJustLeave
                        && position.Equals(model.smallBlockJustLeave.myPos))
                    {
                        continue;
                    }

                    block.IsAbleToStepOn = true;
                    canStepBlocks.Add(block);
                }
            }
        }
    }
}