﻿using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Utility;
using SimpleFramework;
using UnityEngine;

namespace Assets.Scripts
{
    [RequireComponent(typeof(Collider))]
    public class SingleChessBlock : AbstractController
    {
        public CustomPos myPos;

        [SerializeField] private GameObject cursorEffect;
        [SerializeField] private GameObject triggeredEffect;
        [SerializeField] private GameObject cantStepOnEffect;

        private ReviveModel model;

        [SerializeField]
        protected bool _isAbleToStepOn;
    
        public virtual bool IsAbleToStepOn
        {
            get => _isAbleToStepOn;
            set => _isAbleToStepOn = value;
        }

        public ChessBoardType type;
        protected bool isAlreadyTriggered = false;

        private void Awake()
        {
            model = ReviveCore.Get<ReviveModel>();
        }

        private void OnMouseOver()
        {
            if(_isAbleToStepOn && type == ChessBoardType.small)
            {
                SetCursorEffect(true);
                ReviveCore.Get<ReviveModel>().cursorType.Value = CursorType.Click;
            }
        }

        private void OnMouseExit()
        {
            SetCursorEffect(false);
            ReviveCore.Get<ReviveModel>().cursorType.Value = CursorType.Normal;
        }

        public void SetCursorEffect(bool flag)
        {
            cursorEffect.SetActive(flag);
        }
        
        public void SetCantStepOnEffect(bool flag)
        {
            cantStepOnEffect.SetActive(flag);
        }
        
        public void SetTriggeredEffect(bool flag)
        {
            triggeredEffect.SetActive(flag);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (isAlreadyTriggered)
            {
                return;
            }

            if (type == ChessBoardType.small)
            {
                if (other.CompareTag("Player"))
                {
                    PlayerExecution();
                }
            }
            else
            {
                if (other.CompareTag("PlayerDummy"))
                {
                    PlayerDummyExecution(other.transform.gameObject);
                }
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (type == ChessBoardType.small)
            {
                if (other.CompareTag("Player"))
                {
                    isAlreadyTriggered = false;
                    var model = ReviveCore.Get<ReviveModel>();
                    
                    // switch
                    if(model.smallBlockJustLeave!=null)
                    {
                        model.smallTriggeredChessBlocks.Add(model.smallBlockJustLeave);
                        model.smallBlockJustLeave.SetTriggeredEffect(true);
                        model.smallBlockJustLeave.SetCantStepOnEffect(false);
                        model.smallBlockJustLeave.SetCantStepOnEffect(false);
                    }
                    model.smallBlockJustLeave = this;
                    model.smallBlockJustLeave.SetCantStepOnEffect(true);
                }
            }
            else
            {
                if (other.CompareTag("PlayerDummy"))
                {
                    isAlreadyTriggered = false;
                    var model = ReviveCore.Get<ReviveModel>();
                    
                    // // switch
                    // if(model.largeBlockJustLeave)
                    // {
                    //     model.largeTriggeredChessBlocks.Add(model.largeBlockJustLeave);
                    //     model.largeBlockJustLeave.SetTriggeredEffect(true);
                    //     model.largeBlockJustLeave.SetCantStepOnEffect(false);
                    // }
                    // model.largeBlockJustLeave = this;
                    // model.largeBlockJustLeave.SetCantStepOnEffect(true);
                }
            }
        }

        protected virtual void PlayerExecution()
        {
            var model = ReviveCore.Get<ReviveModel>();

            if (model.smallBlockJustLeave != null)
            {
                isAlreadyTriggered = true;
                var movement = new CustomPos()
                {
                    x = myPos.x - model.smallBlockJustLeave.myPos.x,
                    y = myPos.y - model.smallBlockJustLeave.myPos.y
                };

                // QueueIn 命令
                model.commandList.Add(movement);
                model.reversesCommandList.Add(movement);
                
                #region RoundOverEventTrigger

                if (model.commandList.Count >= 4)
                {
                    // 手动去掉不可踩的方块
                    model.smallTriggeredChessBlocks.Add(model.smallBlockJustLeave);
                    model.smallBlockJustLeave.SetCantStepOnEffect(false);
                    model.smallBlockJustLeave.SetTriggeredEffect(true);

                    // List<CustomPos> list = new List<CustomPos>();
                    // while (model.commandList.Count != 0)
                    // {
                    //     var member = model.commandList.First();
                    //     model.commandList.Remove(member);
                    //     list.Add(member);
                    // }
                    //
                    // model.commandList.Clear();
                    ReviveCore.Get<EventSystem>().Trigger(new RoundOverEvent()
                    {
                        commandList = model.commandList
                    });
                }

                #endregion
            }
            
            // 更改model记录的当前玩家位置
            model.curPlayerPos.Value = myPos;
        }

        protected virtual void PlayerDummyExecution(GameObject dummyPlayer = null)
        {
            isAlreadyTriggered = true;
            var model = ReviveCore.Get<ReviveModel>();

            // 更改model记录的当前玩家位置
            model.curPlayerDummyPos.Value = myPos;
        }
    }
}