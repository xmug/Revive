﻿using System;
using System.Collections.Generic;
using SimpleFramework;
using UnityEngine;
using UnityEngine.Serialization;

namespace Assets.Scripts
{
    [Serializable]
    public struct ChessBoardSize
    {
        public int Long;
        public int Width;
    }

    public enum ChessBoardType
    {
        small,
        large
    }
    public class ChessBoardManager : AbstractController
    {
        public ChessBoardType chessBoradType;
        public ChessBoardSize chessBoardSize;
        public SingleChessBlock[] prefabs;
        public List<SingleChessBlock> chessBlocks;

        private void Awake()
        {
            ChessBoardSystem chessBoardSystem = ReviveCore.Get<ChessBoardSystem>();

            foreach (var block in chessBlocks)
            {
                chessBoardSystem.RegisterChessBlock(block, block.type);
            }
        }

        private void BlockInit(SingleChessBlock chessBlock)
        {
        }
    }
}