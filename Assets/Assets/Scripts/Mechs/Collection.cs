using System;
using Assets.Scripts;
using SimpleFramework;
using UnityEngine;

namespace Assets.Scripts
{
    public class Collection : SingleChessBlock
    {
        private bool isCollected = false;
        public GameObject mesh;
        private bool isSaved = false;

        private void Start()
        {
            isCollected = false;
            mesh.SetActive(true);
            ReviveCore.Get<ReviveModel>().levelTmpCollectioNum.Value = 0;
            ReviveCore.Get<ReviveModel>().levelTmpCollectionBase = 0;
            ReviveCore.Get<EventSystem>().Register<DummyDeathEvent>(param =>
            {
                if (!isSaved)
                {
                    ReviveCore.Get<ReviveModel>().levelTmpCollectioNum.Value
                        = ReviveCore.Get<ReviveModel>().levelTmpCollectionBase;
                    isCollected = false;
                    mesh.SetActive(true);
                    isAlreadyTriggered = false;
                }
            });

            ReviveCore.Get<EventSystem>().Register<DataSaveEvent>(param =>
            {
                if (isCollected)
                {
                    ReviveCore.Get<ReviveModel>().levelTmpCollectionBase 
                        = ReviveCore.Get<ReviveModel>().levelTmpCollectioNum.Value;
                    isSaved = true;
                }
            });
        }

        protected override void PlayerDummyExecution(GameObject dummyPlayer = null)
        {
            base.PlayerDummyExecution(dummyPlayer);

            // print(isCollected);
            if (!isCollected)
            {
                // print("123123123");
                ReviveCore.Get<ReviveModel>().levelTmpCollectioNum.Value += 1;
                if (ReviveCore.Get<ReviveModel>().commandList.Count != 0)
                {
                    ReviveCore.Get<EventSystem>().Trigger(new RoundStartEvent());
                }

                isCollected = true;
                mesh.SetActive(false);
            }
        }
    }
}