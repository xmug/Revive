using System;
using Assets.Scripts;
using SimpleFramework;
using UnityEngine;

namespace Assets.Scripts
{
    public class EndPoint : SingleChessBlock
    {
        protected override void PlayerDummyExecution(GameObject dummyPlayer = null)
        {
            base.PlayerDummyExecution(dummyPlayer);
            
            if(ReviveCore.Get<ReviveModel>().commandList.Count!=0)
            {
                ReviveCore.Get<EventSystem>().Trigger(new RoundStartEvent());
            }
            ReviveCore.Get<EventSystem>().Trigger(new LevelOverEvent());
        }
    }
}

