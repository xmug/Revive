using System;
using Assets.Scripts;
using SimpleFramework;
using UnityEngine;

namespace Assets.Scripts
{
    public class Trap : SingleChessBlock
    {
        private void Start()
        {
            ReviveCore.Get<EventSystem>().Register<DummyDeathEvent>(param => { isAlreadyTriggered = false; });
        }

        protected override void PlayerDummyExecution(GameObject dummyPlayer = null)
        {
            base.PlayerDummyExecution(dummyPlayer);
            
            // Destroy(dummyPlayer);
            isAlreadyTriggered = false;
            ReviveCore.Get<EventSystem>().Trigger(new DummyDeathEvent());
        }
    }
}

