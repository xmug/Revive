using System;
using System.Collections;
using SimpleFramework;
using UnityEngine;

namespace Assets.Scripts
{
    public class Escalator : SingleChessBlock
    {
        public Transform targetPos;
        private bool EscalatorBroadCast = false;

        private void Start()
        {
            ReviveCore.Get<EventSystem>().Register<EscalatorEnterEvent>(param => { EscalatorBroadCast = true; });
            ReviveCore.Get<EventSystem>().Register<DummyDeathEvent>(param => { isAlreadyTriggered = false; });
        }

        protected override void PlayerDummyExecution(GameObject dummyPlayer = null)
        {
            base.PlayerDummyExecution(dummyPlayer);

            ReviveCore.Get<EventSystem>().Trigger(new EscalatorEnterEvent());
            EscalatorBroadCast = false;
            StartCoroutine(ForceMovement());
        }

        IEnumerator ForceMovement()
        {
            var model = ReviveCore.Get<ReviveModel>();
            Vector3 dir;
            do
            {
                var dTransform = model.dummyPlayerInstance.transform;
                dir = Vector3.ProjectOnPlane(
                    (targetPos.position - dTransform.position).normalized, dTransform.up);

                dTransform.position += dir * 50f * Time.deltaTime;

                float rotationInRadian = Mathf.Acos(
                    Vector3.Dot(targetPos.position - dTransform.position, dTransform.forward)
                    / ((targetPos.position - dTransform.position).magnitude * dTransform.forward.magnitude));

                float sign = Mathf.Sign(Vector3.Dot(dir, dTransform.right)
                                        / (dir.magnitude * dTransform.right.magnitude));

                dTransform.Rotate(dTransform.up, Mathf.Lerp(0,
                    Mathf.Rad2Deg * rotationInRadian * sign, 2 * Time.deltaTime), Space.World);
                yield return null;
            } while (dir.magnitude > 0.01f && !EscalatorBroadCast);


            if (EscalatorBroadCast != true)
            {
                ReviveCore.Get<EventSystem>().Trigger(new EscalatorLeaveEvent());
            }

            EscalatorBroadCast = false;
        }
    }
}