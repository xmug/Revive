using System;
using Assets.Scripts;
using SimpleFramework;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts
{
    public class StartPoint : SingleChessBlock
    {
        public GameObject prefab;
        public Transform respawnPoint;
        public Transform playerRoot;
        private GameObject dummyInstance;
        private void Awake()
        {
            ReviveCore.Get<EventSystem>().Register<DummyDeathEvent>(param =>
            {
                Destroy(ReviveCore.Get<ReviveModel>().dummyPlayerInstance);
                Generate();
            });
            ReviveCore.Get<ReviveModel>().SavePoint.OnValueChanged += vector3 =>
            {
                respawnPoint.position = vector3;
            };
        }

        private void Start()
        {
            Generate();
            ReviveCore.Get<EventSystem>().Trigger(new LevelStartEvent());
        }

        private GameObject Generate()
        {
            return Instantiate(prefab, respawnPoint.transform.position
                , respawnPoint.transform.rotation, playerRoot.transform);
        }
    }
}

