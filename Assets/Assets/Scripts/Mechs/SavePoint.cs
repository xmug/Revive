using System;
using Assets.Scripts;
using SimpleFramework;
using UnityEngine;

namespace Assets.Scripts
{
    public class SavePoint : SingleChessBlock
    {
        public Transform respawnPoint;
        
        protected override void PlayerDummyExecution(GameObject dummyPlayer = null)
        {
            base.PlayerDummyExecution(dummyPlayer);
            
            ReviveCore.Get<ReviveModel>().SavePoint.Value = respawnPoint.transform.position;
            ReviveCore.Get<ReviveModel>().reversesCommandList.Clear();
            ReviveCore.Get<EventSystem>().Trigger(new RoundStartEvent());
            ReviveCore.Get<EventSystem>().Trigger(new DataSaveEvent());
        }
    }
}

