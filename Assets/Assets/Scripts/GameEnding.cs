using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using UnityEditor;
using UnityEngine;

public class GameEnding : MonoBehaviour
{
    public GameObject NormalEnding;
    public GameObject GoodEnding;

    public void GameEndingStart(int CollectionNumber)
    {
        GetComponent<Canvas>().enabled = true;
        var curNum = PlayerPrefs.GetInt("REVIVE_COLLECTION", 0);
        if (curNum >= CollectionNumber)
        {
            StartCoroutine(Show(GoodEnding));
        }
        else
        {
            StartCoroutine(Show(NormalEnding));
        }
    }

    IEnumerator Show(GameObject ending)
    {
        ending.SetActive(true);
        yield return new WaitForSeconds(5f);
        ending.SetActive(false);
        
        Application.Quit();
    }
}
