using Assets.Scripts;
using SimpleFramework;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CursorManager : AbstractController
{
    public Texture2D clickCursor;
    public Texture2D normalCursor;
    public Texture2D toyCursor;

    private CursorType _cursorType = CursorType.Normal;
    
    private void Awake()
    {
        ReviveCore.Get<ReviveModel>().cursorType.OnValueChanged += (param =>
        {
            if (_cursorType != param)
            {
                switch (param)
                {
                    case CursorType.Click:
                        Cursor.SetCursor(clickCursor, 
                            new Vector2(normalCursor.width/3,clickCursor.width/3),
                            CursorMode.Auto);
                        break;
                    case CursorType.Toy:
                        Cursor.SetCursor(toyCursor, 
                            new Vector2(toyCursor.width/2, toyCursor.height/2),
                            CursorMode.Auto);
                        break;
                    case CursorType.Normal:
                        Cursor.SetCursor(normalCursor, 
                            new Vector2(normalCursor.width/3,normalCursor.height/3),
                            CursorMode.Auto);
                        break;
                    default:
                        Cursor.SetCursor(normalCursor, 
                            new Vector2(normalCursor.width/3,normalCursor.height/3),
                            CursorMode.Auto);
                        break;
                }

                _cursorType = param;
            }
        });
    }
}
