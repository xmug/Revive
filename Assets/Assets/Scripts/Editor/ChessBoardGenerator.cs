using System;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts;
using Assets.Scripts.Utility;
using NUnit.Framework;
using UnityEditor;
using UnityEditor.Playables;
using UnityEngine;

[CustomEditor(typeof(ChessBoardManager))]
public class ChessBoardGenerator : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var target = (ChessBoardManager) (serializedObject.targetObject);
        if (GUILayout.Button("生成"))
        {
            var prefabs = target.prefabs;

            int prefabIndex = 0;
            for (int i = 0; i < target.chessBoardSize.Width; i++)
            {
                for (int j = 0; j < target.chessBoardSize.Long; j++)
                {
                    var prefab = prefabs[prefabIndex];
                    // ++
                    prefabIndex = (prefabIndex + 1) % prefabs.Length;

                    Vector3 sizeOfPrefab = prefab.transform.localScale;

                    // Init
                    var singleChessBlock = Instantiate(prefab, target.transform);
                    singleChessBlock.myPos = new CustomPos()
                    {
                        x = j,
                        y = i
                    };
                    singleChessBlock.transform.localPosition =
                        new Vector3(j * sizeOfPrefab.x, singleChessBlock.transform.localPosition.y,
                            i * sizeOfPrefab.z * -1f);
                    singleChessBlock.type = target.chessBoradType;
                    singleChessBlock.name =
                        "(" + singleChessBlock.myPos.x
                            + ", "
                            + singleChessBlock.myPos.y + ")";

                    // 设置脏值，使得prefab值不会被覆盖
                    EditorUtility.SetDirty(singleChessBlock);

                    // Add
                    target.chessBlocks.Add(singleChessBlock);
                }
                
                if (target.chessBoardSize.Long % 2 == 0)
                    prefabIndex++;

                prefabIndex %= prefabs.Length;
            }
        }

        if (GUILayout.Button("清空"))
        {
            foreach (var chessBlock in target.chessBlocks)
            {
                if (chessBlock)
                    DestroyImmediate(chessBlock.transform.gameObject);
            }

            target.chessBlocks.Clear();
        }

        if (GUILayout.Button("自动重新配置"))
        {
            target.chessBlocks.Clear();
            SingleChessBlock[] blocks = target.transform.GetComponentsInChildren<SingleChessBlock>();
            for (int i = 0; i < target.chessBoardSize.Width; i++)
            {
                for (int j = 0; j < target.chessBoardSize.Long; j++)
                {
                    Debug.Log(i * target.chessBoardSize.Long + j + " x: " + j + " y: " + i);
                    var block = blocks[i * target.chessBoardSize.Long + j];
                    block.myPos.x = Mathf.RoundToInt((block.transform.localPosition.x / block.transform.localScale.x));
                    block.myPos.y = -1 * Mathf.RoundToInt((block.transform.localPosition.z / block.transform.localScale.z));
                    block.transform.localPosition =
                        new Vector3(block.myPos.x * block.transform.localScale.x, block.transform.localPosition.y,
                            block.myPos.y * block.transform.localScale.z * -1f);
                    // 设置脏值，使得prefab值不会被覆盖
                    EditorUtility.SetDirty(block);
            
            
                    target.chessBlocks.Add(block);
                }
            }
        }
    }
}