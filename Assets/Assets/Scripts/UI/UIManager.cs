using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using ParadoxNotion;
using SimpleFramework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public GameObject eventOb;
    public GameObject startGame01;
    public GameObject startGame02;
    public GameObject shezhi;
    public GameObject pause_UI;
    public GameObject shuoming_UI;
    public GameObject passLevel_UI;
    public GameObject GamePass_UI;
    public GameEnding EndingCanvas_Script;
    public LoadingUI Loading_Script;
    public OpeningCGPlayer openCGPlayer;


    public Button start_Btn01;
    public Button back_Btn;

    public Button continue_Btn;
    public Button exit_Btn;
    public Button exit_Btn2;

    public Button start_GameBtn;
    public Button shezhi_Btn;
    public Button back_ToGameBtn;

    public Button back_ToCunDangBtn;
    public Button restart_GameBtn;
    public Button shuoming_Btn;
    public Button back_ToMainMenuBtn;

    public Button back_ToPauseBtn;

    public Button next_LevelBtn;

    public Button continueBtn;

    public Button FastRetryBtn;

    public Text maxNumText;
    public Text curNumText;
    public int CollectionNumber;
    private bool isCGPlayed = false;

    private static UIManager _instance;

    public static UIManager Instance
    {
        get { return _instance; }
    }


    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(_instance);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        startGame01.gameObject.SetActive(true);
        maxNumText.text = "/ " + CollectionNumber;
        curNumText.text = PlayerPrefs.GetInt("REVIVE_COLLECTION", 0).ToString();

        continueBtn.onClick.AddListener(BackToGame);
        start_Btn01.onClick.AddListener(StartGame01); //加载第二张图片
        back_Btn.onClick.AddListener(BackToGame01); //回到第一张图片
        continue_Btn.onClick.AddListener(ContinueGame); //继续游戏
        exit_Btn.onClick.AddListener(ExitGame); //退出游戏 √
        exit_Btn2.onClick.AddListener(GameEndingExit); //退出游戏 √
        start_GameBtn.onClick.AddListener(StartGame02); //开始游戏画面
        shezhi_Btn.onClick.AddListener(SheZhi); //打开设置UI
        back_ToGameBtn.onClick.AddListener(BackToGame); //关闭暂停UI，返回游戏
        back_ToCunDangBtn.onClick.AddListener(BackToCunDang); //返回存档点 √
        FastRetryBtn.onClick.AddListener(BackToCunDang);
        restart_GameBtn.onClick.AddListener(RestartGame); //重新开始本关 √
        shuoming_Btn.onClick.AddListener(ShuoMing); // 打开游戏说明 √
        back_ToPauseBtn.onClick.AddListener(BackToPauseUI); //  从说明UI返回到暂停UI √
        back_ToMainMenuBtn.onClick.AddListener(BackToMainMenu); //  返回主菜单 √
        next_LevelBtn.onClick.AddListener(NextLevel); // 继续下一关 √

        SceneManager.sceneLoaded += (arg0, mode) =>
        {
            ReviveCore.Get<EventSystem>().Register<LevelOverEvent>(param =>
            {
                if (SceneManager.GetActiveScene().buildIndex == SceneManager.sceneCountInBuildSettings - 1)
                {
                    // 最后一关结束了
                    GameOver();
                }
                else
                {
                    PassLevel();
                }
            });

            ReviveCore.Get<EventSystem>().Register<SceneSwitchEvent>(param =>
            {
                shezhi.SetActive(true);
                Time.timeScale = 1;
                ReviveCore.Get<ReviveModel>().gameIsPaused.Value = false;
            });

            ReviveCore.Get<ReviveModel>().levelTmpCollectioNum.OnValueChanged += i =>
            {
                var num = (PlayerPrefs.GetInt("REVIVE_COLLECTION", 0) + i);

                curNumText.text = (num > CollectionNumber ? CollectionNumber : num).ToString();
            };
        };

        ReviveCore.Get<EventSystem>().Register<CGPlayOver>(over => { startGame02.gameObject.SetActive(true); });
    }


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Time.timeScale == 1)
            {
                SheZhi();
            }
            else
            {
                BackToGame();
            }
        }
    }

    private void GameEndingExit()
    {
        EndingCanvas_Script.GameEndingStart(CollectionNumber);
    }

    private void LoadScene(int index)
    {
        var async = SceneManager.LoadSceneAsync(index);
        Loading_Script.StartLoading(ref async);
    }

    //到达终点后，调用该方法，显示通过本关的UI（演出完成！）
    public void PassLevel()
    {
        shezhi.SetActive(false);
        passLevel_UI.SetActive(true);
    }

    public void GameOver()
    {
        shezhi.SetActive(false);
        GamePass_UI.SetActive(true);
    }


    private void StartGame01() //加载第二张图片
    {
        startGame01.gameObject.SetActive(false);
        if(!isCGPlayed)
        {
            openCGPlayer.StartFuckingVideo();
            isCGPlayed = true;
        }
        else
        {
            startGame02.gameObject.SetActive(true);
        }
    }

    private void BackToGame01() //回到第一张图片
    {
        startGame02.gameObject.SetActive(false);
        startGame01.gameObject.SetActive(true);
    }


    private void SheZhi() //打开设置
    {
        shezhi.SetActive(false);
        pause_UI.SetActive(true);

        Pause();
    }

    private void BackToGame() //返回游戏画面
    {
        pause_UI.SetActive(false);
        shezhi.SetActive(true);

        Pause();
    }

    private void Pause()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
            ReviveCore.Get<ReviveModel>().gameIsPaused.Value = true;
        }
        else
        {
            Time.timeScale = 1;
            ReviveCore.Get<ReviveModel>().gameIsPaused.Value = false;
        }
    }

    private void ExitGame() //退出游戏
    {
        //退出游戏........
        Application.Quit();
    }

    private void ContinueGame() // 继续上一次保存的
    {
        startGame02.gameObject.SetActive(false);
        //游戏场景。。。。出现 
        int lastLevel = PlayerPrefs.GetInt("LevelStorage");
        if (lastLevel == 0 || lastLevel == SceneManager.sceneCountInBuildSettings)
        {
            // 没玩过或者通关了
            LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
        else
        {
            // 没玩过
            LoadScene(lastLevel);
        }
    }

    private void StartGame02() // 重新开始游戏
    {
        // 重置收集品
        ReviveCore.Get<ReviveModel>().numOfCollection.Value = 0;
        startGame02.gameObject.SetActive(false);
        PlayerPrefs.SetInt("REVIVE_COLLECTION", 0);
        curNumText.text = PlayerPrefs.GetInt("REVIVE_COLLECTION", 0).ToString();
        //游戏场景。。。。出现 
        LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    private void BackToCunDang() //返回存档点
    {
        if (!ReviveCore.Get<ReviveModel>().resetLock)
        {
            pause_UI.SetActive(false);
            shezhi.SetActive(true);
            //角色回到存档点......
            Time.timeScale = 1;
            ReviveCore.Get<ReviveModel>().gameIsPaused.Value = false;
            ReviveCore.Get<EventSystem>().Trigger(new DummyDeathEvent());
        }
    }

    private void RestartGame() //重新开始本关
    {
        pause_UI.SetActive(false);
        shezhi.SetActive(false);
        // 重新加载本关 。。。。。
        Pause();
        LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void ShuoMing() // 打开游戏说明
    {
        pause_UI.SetActive(false);
        shuoming_UI.SetActive(true);
    }

    private void BackToPauseUI() //  从说明UI返回到暂停UI
    {
        shuoming_UI.SetActive(false);
        pause_UI.SetActive(true);
    }

    private void BackToMainMenu() //  返回主菜单
    {
        pause_UI.SetActive(false);
        shezhi.SetActive(false);
        startGame01.SetActive(true);
        Time.timeScale = 1;
        ReviveCore.Get<ReviveModel>().gameIsPaused.Value = false;

        SceneManager.LoadScene(0);
    }

    private void NextLevel() // 继续下一关
    {
        //点击继续后
        passLevel_UI.SetActive(false);
        //加载下一关....
        Debug.Log("成功加载下一关！");
        LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}