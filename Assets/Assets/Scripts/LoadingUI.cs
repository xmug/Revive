using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using SimpleFramework;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingUI : MonoBehaviour
{
    public Canvas canvas;
    public Image bg;
    public Image butterfly;
    public Image processBar;
    private float curProcess = 8f;
    private float bar_Speed = 30f;
    private float butterfly_Speed = 190f;
    private AsyncOperation async;

    public void StartLoading(ref AsyncOperation operation)
    {
        canvas.enabled = true;
        ReviveCore.Get<ReviveModel>().cursorType.Value = CursorType.Toy;
        async = operation;
        async.allowSceneActivation = false;
        StartCoroutine(Loading());
    }

    IEnumerator Loading()
    {
        float maxProcess;
        do
        {
            if(Input.GetMouseButtonDown(0))
                ReviveCore.Get<EventSystem>().Trigger(new ToySoundEvent());
            Vector3 curX = butterfly.transform.localPosition;
            maxProcess = 100f;
            if (curProcess < maxProcess)
            {
                curProcess += Time.deltaTime * bar_Speed;
            }

            if (curX.x < 260f)
            {
                curX.x += Time.deltaTime * butterfly_Speed;
                butterfly.transform.localPosition = curX;
            }

            processBar.fillAmount = curProcess / 100f;

            if (curProcess > 99.25f)
            {
                async.allowSceneActivation = true;
            }

            yield return new WaitForFixedUpdate();
        } while (curProcess < maxProcess);
        
        ReviveCore.Get<EventSystem>().Trigger(new SceneSwitchEvent());
        ReviveCore.Get<ReviveModel>().cursorType.Value = CursorType.Normal;
        curProcess = 50f;
        canvas.enabled = false;
    }
}