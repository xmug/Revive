﻿using System.Collections.Generic;
using Assets.Scripts.Utility;
using FuckingEnum;
using SimpleFramework;
using UnityEngine;

namespace Assets.Scripts
{
    public class ReviveModel : IModel
    {
        public BindableProperty<BlackBoardEnum.DialogueState> dialogueState =
            new BindableProperty<BlackBoardEnum.DialogueState>()
            {
                Value = BlackBoardEnum.DialogueState.Pause
            };

        public BindableProperty<bool> gameIsPaused = new BindableProperty<bool>()
        {
            Value = false
        };

        public BindableProperty<CustomPos> curPlayerPos = new BindableProperty<CustomPos>()
        {
            Value = new CustomPos()
            {
                x = int.MinValue,
                y = int.MinValue
            }
        };

        public BindableProperty<CustomPos> curPlayerDummyPos = new BindableProperty<CustomPos>()
        {
            Value = new CustomPos()
            {
                x = int.MinValue,
                y = int.MinValue
            }
        };

        public List<CustomPos> commandList = new List<CustomPos>();
        
        // Store all the commands from the last save point
        public List<CustomPos> reversesCommandList = new List<CustomPos>();

        public List<SingleChessBlock> smallCanStepBlocks = new List<SingleChessBlock>();
        public List<SingleChessBlock> largeCanStepBlocks = new List<SingleChessBlock>();

        public SingleChessBlock smallBlockJustLeave;
        public SingleChessBlock largeBlockJustLeave;
        public List<SingleChessBlock> smallTriggeredChessBlocks = new List<SingleChessBlock>();
        public List<SingleChessBlock> largeTriggeredChessBlocks = new List<SingleChessBlock>();

        public BindableProperty<Vector3> SavePoint = new BindableProperty<Vector3>()
        {
            Value = Vector3.zero
        };

        public BindableProperty<CursorType> cursorType = new BindableProperty<CursorType>()
        {
            Value = CursorType.Normal
        };

        public BindableProperty<int> numOfCollection = new BindableProperty<int>()
        {
            Value = PlayerPrefs.GetInt("REVIVE_COLLECTION", 0)
        };

        public BindableProperty<int> levelTmpCollectioNum = new BindableProperty<int>()
        {
            Value = 0
        };

        public int levelTmpCollectionBase = 0;

        public GameObject dummyPlayerInstance;

        public bool resetLock = false;

        public bool IsInit { get; set; }

        public void Init()
        {
            ReviveCore.Get<EventSystem>().Register<RoundStartEvent>(param =>
            {
                List<SingleChessBlock> blocks = new List<SingleChessBlock>();
                blocks.AddRange(smallTriggeredChessBlocks);
                // blocks.AddRange(largeTriggeredChessBlocks);
                foreach (var singleChessBlock in blocks)
                {
                    singleChessBlock.SetTriggeredEffect(false);
                }

                if (smallBlockJustLeave != null)
                {
                    smallBlockJustLeave.SetCantStepOnEffect(false);
                    // largeBlockJustLeave.SetCantStepOnEffect(false);
                    smallBlockJustLeave.IsAbleToStepOn = true;
                    // largeBlockJustLeave.IsAbleToStepOn = true;
                    smallBlockJustLeave = null;
                }

                // largeBlockJustLeave = null;
                smallTriggeredChessBlocks.Clear();
                largeTriggeredChessBlocks.Clear();
            });

            numOfCollection.OnValueChanged += i =>
            {
                PlayerPrefs.SetInt("REVIVE_COLLECTION", i);
            };
            
            ReviveCore.Get<EventSystem>().Register<LevelOverEvent>(param =>
            {
                numOfCollection.Value += levelTmpCollectioNum.Value;
            });
        }
    }
}