﻿using System;
using System.Collections;
using System.Numerics;
using Assets.Scripts.Utility;
using SimpleFramework;
using UnityEngine;
using Quaternion = UnityEngine.Quaternion;
using Vector3 = UnityEngine.Vector3;


namespace Assets.Scripts.PlayerControll
{
    [RequireComponent(typeof(Rigidbody))]
    [RequireComponent(typeof(Collider))]
    public class AController : AbstractController
    {
        public CustomPos curPos;
        public float speed;
        protected Rigidbody rb;
        protected ReviveModel model;
        protected bool isMoving = false;
        protected EventSystem eSys;
        [SerializeField] private float rotationSpeed;
        [SerializeField] private float jumpingHight;
        [SerializeField] private float jumpingSpeed;
        [SerializeField] private float gravityParameter;

        protected virtual void Awake()
        {
            // 引用
            model = ReviveCore.Get<ReviveModel>();
            eSys = ReviveCore.Get<EventSystem>();
            rb = GetComponent<Rigidbody>();
        }
        
        protected virtual void Start()
        {
            curPos = model.curPlayerPos.Value;
        }

        protected IEnumerator PlayerMovingProcess(Vector3 target, float angle)
        {
            Vector3 dir = Vector3.zero;
            
            float heightSub;
            float targetHeight = transform.position.y + jumpingHight;
            
            // 开启飞行模式
            rb.useGravity = false;
            do
            {
                var position = transform.position;
                heightSub = targetHeight - position.y;
                position = new Vector3(position.x,
                    Mathf.Lerp(position.y, targetHeight, Time.deltaTime * jumpingSpeed), position.z);
                transform.position = position;
                yield return null;
            } while (heightSub > 2f);
            
            // 关闭飞行模式
            rb.useGravity = true;
            do
            {
                var position = transform.position;
                dir = Vector3.ProjectOnPlane(
                    (target - position).normalized, transform.up);
                rb.position += dir * speed * Time.deltaTime;
                rb.position += transform.up * -1 * gravityParameter * Time.deltaTime;
                
                float rotationInRadian = Mathf.Acos(Vector3.Dot(target - position, transform.forward) 
                                                   / ((target - position).magnitude * transform.forward.magnitude));
               
                float sign = Mathf.Sign(Vector3.Dot(dir, transform.right)
                                       / (dir.magnitude * transform.right.magnitude));
                
                transform.Rotate(transform.up, Mathf.Lerp(0, 
                    Mathf.Rad2Deg*rotationInRadian * sign, rotationSpeed * Time.deltaTime), Space.World);
                
                yield return null;
            } while (dir.magnitude > 0.1f);

            isMoving = false;
            model.resetLock = false;
        }
    }
}