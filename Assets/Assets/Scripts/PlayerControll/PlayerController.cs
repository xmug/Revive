﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Utility;
using ParadoxNotion;
using SimpleFramework;
using UnityEngine;
using UnityEngine.SceneManagement;
using Vector3 = UnityEngine.Vector3;

namespace Assets.Scripts.PlayerControll
{
    public class PlayerController : AController
    {
        private bool RoundNotOver = true;
        public LayerMask hitLayer;
        private bool isReversing = false;
        private CustomPos[] commandCopy;
        private int index;

        protected override void Awake()
        {
            base.Awake();

            model.curPlayerPos.OnValueChanged += pos => curPos = pos;
            // 事件注册
            eSys.Register<RoundOverEvent>(param => { RoundNotOver = false; });
            eSys.Register<RoundStartEvent>(param =>
            {
                RoundNotOver = true;
                model.commandList.Clear();
            });
            eSys.Register<DummyDeathEvent>(param =>
            {
                // 重置位置
                commandCopy = new CustomPos[model.reversesCommandList.Count];
                model.reversesCommandList.CopyTo(commandCopy);
                index = commandCopy.Length - 1;
                // 手动取消一次
                if(model.smallBlockJustLeave!=null)
                {
                    model.smallBlockJustLeave.IsAbleToStepOn = true;
                }
                // 清除大记录
                model.reversesCommandList.Clear();
                isReversing = true;
            });

            // 简易存档
            PlayerPrefs.SetInt("LevelStorage", SceneManager.GetActiveScene().buildIndex);
        }

        private void OnDestroy()
        {
            // 取消注册
            model.curPlayerPos.OnValueChanged -= pos => curPos = pos;
            // eSys.UnRegister<RoundOverEvent>(param => { RoundNotOver = false; });
            // eSys.UnRegister<RoundStartEvent>(param => { RoundNotOver = true; });
            //
            // eSys.UnRegister<DummyDeathEvent>(param =>
            // {
            //     RoundNotOver = true;
            //     model.commandList.Clear();
            // });
        }

        private void Update()
        {
            ReverseMovement();
            MouseDetector();
        }

        private void MouseDetector()
        {
            if (Input.GetMouseButtonDown(0) && RoundNotOver && !isMoving)
            {
                if (Physics.Raycast
                    (Camera.main.ScreenPointToRay(Input.mousePosition), out RaycastHit hitInfo, 10000.0f, hitLayer))
                {
# if UNITY_EDITOR
                    Debug.DrawLine(Camera.main.ScreenPointToRay(Input.mousePosition).origin,
                        Camera.main.ScreenPointToRay(Input.mousePosition).origin +
                        Camera.main.ScreenPointToRay(Input.mousePosition).direction * 10000, Color.red, 5.0f);
# endif
                    // 拿到方块身上的属性
                    var blockAttr = hitInfo.transform.GetComponent<SingleChessBlock>();
                    if (blockAttr != null && blockAttr.type == ChessBoardType.small && blockAttr.IsAbleToStepOn)
                    {
                        // 查询世界坐标
                        var targetPos = ReviveCore.Get<ChessBoardSystem>()
                            .GetChessBlock(blockAttr.myPos, ChessBoardType.small);

                        var movement = new CustomPos()
                        {
                            x = blockAttr.myPos.x - model.curPlayerPos.Value.x,
                            y = blockAttr.myPos.y - model.curPlayerPos.Value.y
                        };

                        var curRealPos = ReviveCore.Get<ChessBoardSystem>()
                            .GetChessBlock(curPos, ChessBoardType.small);

                        if (targetPos != null)
                        {
                            // 开始移动
                            StartCoroutine(PlayerMovingProcess
                                (targetPos.transform.position, 0));
                            isMoving = true;
                            model.resetLock = true;
                        }
                    }
                }
            }
        }

        private void ReverseMovement()
        {
            if (isReversing)
            {
                if (!isMoving)
                {
                    if (index < 0)
                    {
                        eSys.Trigger(new RoundStartEvent());
                        isReversing = false;
                        return;
                    }
                    
                    // 粗暴清除
                    model.commandList.Clear();

                    var curCommand = commandCopy[index--];

                    var targetBlockPos = new CustomPos()
                    {
                        x = curPos.x - curCommand.x,
                        y = curPos.y - curCommand.y
                    };

                    // 查询世界坐标
                    var targetRealPos = ReviveCore.Get<ChessBoardSystem>()
                        .GetChessBlock(targetBlockPos, ChessBoardType.small, true);

                    if (targetRealPos != null)
                    {
                        StartCoroutine(ReverseMove(targetRealPos.transform.position));
                    }
                }
            }
        }



        IEnumerator ReverseMove(Vector3 target)
        {
            Vector3 dir;
            do
            {
                var position = transform.position;
                isMoving = true;
                model.resetLock = true;
                dir = Vector3.ProjectOnPlane(
                    (target - position).normalized, transform.up);
                rb.position += dir * speed * Time.deltaTime;

                float rotationInRadian = Mathf.Acos(Vector3.Dot(target - position, transform.forward)
                                                    / ((target - position).magnitude * transform.forward.magnitude));

                float sign = Mathf.Sign(Vector3.Dot(dir, transform.right)
                                        / (dir.magnitude * transform.right.magnitude));

                transform.Rotate(transform.up, Mathf.Lerp(0,
                    Mathf.Rad2Deg * rotationInRadian * sign, 2 * Time.deltaTime), Space.World);
                yield return null;
            } while (dir.magnitude > 0.1f);
            
            // 粗暴清除
            model.reversesCommandList.Clear();
            isMoving = false;
            model.resetLock = false;
        }
    }
}