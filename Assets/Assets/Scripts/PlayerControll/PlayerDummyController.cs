﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Utility;
using SimpleFramework;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

namespace Assets.Scripts.PlayerControll
{
    public class PlayerDummyController : AController
    {
        private List<CustomPos> commandList;

        private bool dummyRoundStart = false;
        private bool isEscalating = false;

        // private CustomPos curCommand;
        private SingleChessBlock curTargetBlock;
        
        private int curIndex = 0;
        private int oriIndex = 0;

        protected override void Awake()
        {
            base.Awake();

            // 事件注册
            ReviveCore.Get<ReviveModel>().dummyPlayerInstance = this.gameObject;

            ReviveCore.Get<EventSystem>().Register<RoundOverEvent>(param =>
            {
                dummyRoundStart = true;
                curIndex = 0;
                oriIndex = 0;
                commandList = param.commandList;
            });

            ReviveCore.Get<EventSystem>().Register<RoundStartEvent>(param =>
            {
                // 重置
                dummyRoundStart = false;
                isMoving = false;
                curIndex = 0;
                oriIndex = 0;
            });

            ReviveCore.Get<EventSystem>().Register<DummyDeathEvent>(param =>
            {
                dummyRoundStart = false;
                isMoving = false;
                curIndex = 0;
                oriIndex = 0;
            });
            
            ReviveCore.Get<EventSystem>().Register<EscalatorEnterEvent>(param =>
            {
                isEscalating = true;
            });
            
            ReviveCore.Get<EventSystem>().Register<EscalatorLeaveEvent>(param =>
            {
                isMoving = false;
                isEscalating = false;
                curTargetBlock = null;
            });
        }

        private void Update()
        {
            if (dummyRoundStart && !isEscalating)
            {
                // 尝试拿到一条命令
                if (!isMoving)
                {
                    oriIndex = curIndex;
                    do
                    {
                        var index = curIndex++ % commandList.Count;
                        curIndex %= commandList.Count;

                        var curCommand = commandList[index];

                        var targetBlockPos = new CustomPos()
                        {
                            x = model.curPlayerDummyPos.Value.x + curCommand.x,
                            y = model.curPlayerDummyPos.Value
                                .y + curCommand.y
                        };
                        
                        // print(index + " " + curIndex);
                        // targetBlockPos.Print();

                        // 查询世界坐标
                        curTargetBlock = ReviveCore.Get<ChessBoardSystem>()
                            .GetChessBlock(targetBlockPos, ChessBoardType.large);
                        
                        if (oriIndex == curIndex && curTargetBlock==null)
                        {
                            eSys.Trigger(new DummyDeathEvent());
                            break; // 无路可走了
                        }
                        //
                        // if(curTargetBlock!=null) print(curTargetBlock.transform.position);
                    } while (curTargetBlock == null);
                }

                // 有目的地就前往，意味着只要有指令就继续
                if (curTargetBlock != null)
                {
                    DummyMovement(curTargetBlock.transform.position);
                }
            }
        }

        private void DummyMovement(Vector3 target)
        {
            var position = transform.position;
            isMoving = true;
            Vector3 dir = Vector3.ProjectOnPlane(
                (target - position).normalized, transform.up);
            rb.position += dir * speed * Time.deltaTime;

            float rotationInRadian = Mathf.Acos(Vector3.Dot(target - position, transform.forward)
                                                / ((target - position).magnitude * transform.forward.magnitude));

            float sign = Mathf.Sign(Vector3.Dot(dir, transform.right)
                                    / (dir.magnitude * transform.right.magnitude));

            transform.Rotate(transform.up, Mathf.Lerp(0,
                Mathf.Rad2Deg * rotationInRadian * sign, 2 * Time.deltaTime), Space.World);


            if (dir.magnitude < 0.1f)
                isMoving = false;
        }
    }
}