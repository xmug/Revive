﻿using System;
using SimpleFramework;
using UnityEngine;

namespace Assets.Scripts
{
    public struct DataSaveEvent : IEvent<DataSaveEvent>
    {
    }
}