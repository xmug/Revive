﻿using System;
using SimpleFramework;
using UnityEngine;

namespace Assets.Scripts
{
    public struct EscalatorEnterEvent : IEvent<EscalatorEnterEvent>
    {
    }
}