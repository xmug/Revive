﻿using System;
using SimpleFramework;
using UnityEngine;

namespace Assets.Scripts
{
    public enum CursorType
    {
        Normal,
        Click,
        Toy
    }
    
    public struct CursorChange : IEvent<CursorChange>
    {
        public CursorType type;
    }
}