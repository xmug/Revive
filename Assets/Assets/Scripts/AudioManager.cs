using System;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts;
using SimpleFramework;
using UnityEngine;
using Random = UnityEngine.Random;

public class AudioManager : MonoBehaviour
{
    private EventSystem eSys;

    public AudioClip[] dummyStepsAudioClips;
    public AudioClip lose;
    public AudioClip win;
    public AudioClip playAgain;
    public AudioClip startGame;
    public AudioClip[] musicSigns;
    public AudioClip whisper;

    [SerializeField] private AudioSource BGM;

    private AudioSource stepSoundSource;
    private AudioSource eventSource;

    private bool isMoving = false;
    private bool startMoving = false;
    
    [Tooltip("Item Sound")]
    public AudioClip collect;

    private void Awake()
    {
        stepSoundSource = gameObject.AddComponent<AudioSource>();
        eventSource = gameObject.AddComponent<AudioSource>();
        eSys = ReviveCore.Get<EventSystem>();

        ReviveCore.Get<ReviveModel>().gameIsPaused.OnValueChanged += b =>
        {
            if (b)
            {
                BGM.Stop();
                StartCoroutine(SoundPlayer(whisper));
            }
            else
            {
                BGM.Play();
                stepSoundSource.Play();
                eventSource.Stop();
            }
        };
        
        eSys.Register<ToySoundEvent>(param =>
        {
            ToySoundRandomPlayOnce();
        });

        eSys.Register<RoundOverEvent>(param =>
        {
            StartCoroutine(BGMGoingDown(true, 0.65f));
            isMoving = true;
            startMoving = true;
        });
        eSys.Register<RoundStartEvent>(param =>
        {
            StartCoroutine(BGMGoingDown(false, 1f));
            isMoving = false;
        });
        eSys.Register<DummyDeathEvent>(param =>
        {
            isMoving = false;
            StartCoroutine(SoundPlayer(lose));
        });
        eSys.Register<LevelOverEvent>(param => { StartCoroutine(SoundPlayer(win)); });
        eSys.Register<LevelStartEvent>(param => { StartCoroutine(SoundPlayer(playAgain)); });

        ReviveCore.Get<ReviveModel>().levelTmpCollectioNum.OnValueChanged += i =>
        {
            if(i !=  ReviveCore.Get<ReviveModel>().levelTmpCollectionBase)
                StartCoroutine(SoundPlayer(collect));
        };
    }

    // Update is called once per frame
    void Update()
    {
        if (startMoving) StartCoroutine(DummyStep());
    }

    IEnumerator DummyStep()
    {
        startMoving = false;

        while(isMoving)
        {
            if (!stepSoundSource.isPlaying)
            {
                yield return new WaitForSeconds(Random.Range(0.0f, 0.25f));
                int randomNum = Random.Range(0, dummyStepsAudioClips.Length);
                stepSoundSource.clip = dummyStepsAudioClips[randomNum];
                // stepSoundSource.pitch = Random.Range(0f, 1f);
                stepSoundSource.Play();
            }

            yield return null;
        }
    }
    
    public void ToySoundRandomPlayOnce()
    {
        StartCoroutine(SoundPlayer(musicSigns[Random.Range(0, musicSigns.Length - 1)]));
    }

    IEnumerator BGMGoingDown(bool isDown, float target)
    {
        while (Math.Abs(BGM.volume - target) > 0.05f)
        {
            BGM.volume = Mathf.Lerp(BGM.volume, 
                target, Time.deltaTime);
            yield return null;
        }
    }

    IEnumerator SoundPlayer(AudioClip clip)
    {
        // bool tmp = false;
        // do
        // {
        //     if (!tmp)
        //     {
        //         eventSource.clip = clip;
        //         // stepSoundSource.pitch = Random.Range(0f, 1f);
        //         eventSource.Play();
        //         tmp = true;
        //     }
        //
        //     yield return null;
        // } while (eventSource.isPlaying);
        eventSource.Stop();
        eventSource.clip = clip;
        // stepSoundSource.pitch = Random.Range(0f, 1f);
        eventSource.Play();
        yield return null;
    }
}