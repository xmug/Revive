﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.Scripts.Utility
{
    [Serializable]
    public struct CustomPos : IEquatable<CustomPos>
    {
        public int x;
        public int y;

        public bool Equals(CustomPos other)
        {
            return x == other.x && y == other.y;
        }

        public override bool Equals(object obj)
        {
            return obj is CustomPos other && Equals(other);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (x * 397) ^ y;
            }
        }

        public void Print()
        {
            Debug.Log(x + " ," + y);
        }
    }
}