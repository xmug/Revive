﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Assets.Scripts.Utility
{
    public class Queue2Struct : IEquatable<Queue2Struct>
    {
        public Queue<CustomPos> queue;

        public Queue2Struct()
        {
            queue = new Queue<CustomPos>();
        }

        public bool Equals(Queue2Struct other)
        {
            return Equals(queue.Count, other.queue.Count);
        }

        public override bool Equals(object obj)
        {
            return obj is Queue2Struct other && Equals(other);
        }

        public override int GetHashCode()
        {
            return (queue != null ? queue.GetHashCode() : 0);
        }
    }
}