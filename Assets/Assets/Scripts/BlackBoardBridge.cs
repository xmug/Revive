using Assets.Scripts;
using FuckingEnum;
using UnityEngine;

public class BlackBoardBridge : MonoBehaviour
{
    public BlackBoardEnum.DialogueState dialogueState;
    
    private void Awake()
    {
        ReviveCore.Get<ReviveModel>().dialogueState.OnValueChanged += state =>
        {
            dialogueState = state;
        };
    }

    public void SetState(BlackBoardEnum.DialogueState state)
    {
        ReviveCore.Get<ReviveModel>().dialogueState.Value = state;
    }
}
