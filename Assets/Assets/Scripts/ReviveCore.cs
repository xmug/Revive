﻿using SimpleFramework;
using UnityEngine;

namespace Assets.Scripts
{
    public class ReviveCore : BaseCore<ReviveCore>
    {
        protected override void Init()
        {
            Register(new EventSystem());
            Register(new ChessBoardSystem());
            Register(new ReviveModel());
        }
    }
}